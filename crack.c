#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *g = md5(guess, strlen(guess));
    
    // Compare the two hashes
    if (strcmp(g, hash) == 0)
    {
        return 1;   
    }
    else {
        return 0;
    }
    
    // Free any malloc'd memory
    free(g);
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.

char **readfile(char *filename)
{   
    //Open the named file
    FILE *f = fopen(filename, "r");
    int size = 50;
    char **pwds = (char **)malloc(size * sizeof(char *));
    
    char str[40];
    int i = 0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        // If array is full, make it bigger
        if (i == size)
        {
            size = size + 10;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc((strlen(str)+1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;      
    }
    return pwds;
    fclose(f);
    free(pwds);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile("hashes.txt");

    // Read the dictionary file into an array of strings
    char **dict = readfile("rockyou100.txt");

    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    for (int i = 0; i < 20; i++)
    {
        printf("%s  ", hashes[i]);
        for (int k = 0; k < 100; k++)
        {
            tryguess(hashes[i], dict[k]);
            if (tryguess != 0)
            {
                printf("%s\n", dict[k]);
            }            
        }
        
    }
    free(*dict);
    free(*hashes);
    
}
